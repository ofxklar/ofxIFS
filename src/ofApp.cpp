#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
  ofSetFrameRate(60);
  width = 1080;
  height = 1080;
  render.allocate(width, height, OF_IMAGE_COLOR);
  // a = -1.4; 
  // b = 2.3; 
  // c = 2.2; 
  // d = -2.7;

  gui.setup("deJong"); // most of the time you don't need a name
  
  gui.add(a.setup("a", -2, -3.14, 3.14));
  gui.add(b.setup("b", -2, -3.14, 3.14));
  gui.add(c.setup("c", -1.2, -3.14, 3.14));
  gui.add(d.setup("d", 2, -3.14, 3.14));

  gui.add(e.setup("e", 1, 0, 1));
  gui.add(f.setup("f", 1, 0, 1));
  gui.add(g.setup("g", 1, 0, 1));
  gui.add(h.setup("h", 1, 0, 1));

  gui.add(mode_col.set("color", false));
  gui.add(hue_min.setup("hue min", 170, 0, 255));
  gui.add(hue_max.setup("hue max", 200, 0, 255));

  gui.add(n_points.setup("n_points", 500000, 100, 1000000));
  gui.add(decrease.setup("decrease", 0.5, 0, 1.0));
  gui.add(fps.setup("fps", 0, 0, 60));

  //  gui.setup(parameters);

  sync.setup((ofParameterGroup&)gui.getParameter(),6666,"localhost",6667);

  a = -1;
  b = -2;
  c = -1.2;
  d = 2 ;

  x = 0;//ofRandom(-2, 2);
  y = 0;//ofRandom(-2, 2);
  
  setupAudio();
}

ofVec2f ofApp::DeJong(float x, float y){
  return ofVec2f(e*sin(at*y) - f*cos(bt*x), g*sin(ct*x) - ht*cos(dt*y)) ;
}

//--------------------------------------------------------------
void ofApp::update() {
  float t = ofGetElapsedTimef();
  sync.update();
  // int t = ofTime::getAsMilliseconds();

  x = x + ofRandom(-0.001, 0.001);
  y = y + ofRandom(-0.001, 0.001);

  at = a + 0.3 * sin(t);
  bt = b + 0.3 * sin(t * 0.7);
  ct = c + 0.3 * sin(t * 1.2);
  dt = d + 0.3 * sin(t * 0.5);
  // ht = 0.2 * sin(t * 0.6);
  // a += 0.05 * sin(t);

  ofPixels &pix = render.getPixelsRef();

  for (int i = 0; i < pix.size(); ++i) {
    //      pix[i] = max(0, pix[i]-decrease);
    pix[i] = pix[i] * decrease;
  }

  for (int i = 0; i < n_points; ++i) {
    loc_x = x * width;
    loc_y = y * height;

    out = DeJong(x, y);
    x = out.x;
    y = out.y;
    ind = pix.getPixelIndex((out.x + 2.0f) * width / 4.0f,
                            (out.y + 2.0f) * height / 4.0f);
    if (mode_col) {
      ofColor act_col = pix.getColor(ind);
      act_col.setHsb(
          max(float(hue_min), min(float(hue_max), act_col.getHue() + 10)), 255,
          255);

      pix.setColor(ind, act_col);
    } else {
      pix.setColor(ind, ofColor(255));
    }
  }

  //render.setFromPixels(pix);
  render.update();
}

//--------------------------------------------------------------
void ofApp::draw(){
  ofBackground(0);
  fps = ofGetFrameRate();
  ofLog() << ofGetFrameRate();
  render.draw((1920-1080)/2, 0, 1080, 1080);
  drawAudio();
  drawGrab();
  gui.draw();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
  switch(key){
  case 'f':
    ofToggleFullscreen();
    break;
  case 'a':
    mode_audio = !mode_audio;
    break;
  }
   
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
  center.set(ofGetWidth()/2.0f, ofGetHeight()/2.0f);
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}

///////////
// AUDIO //
///////////

//--------------------------------------------------------------
void ofApp::setupAudio(){

  gui.add(draw_audio.set("draw audio", false));
  gui.add(draw_grab.set("draw grab", false));
  gui.add(mode_audio.set("mode audio", false));
  
  bufferSize    = 512;
  sampleRate    = 44100;
  volume        = 0.5f;

  soundStream.setup(this, 2, 0, sampleRate, bufferSize, 4);

  lAudio.assign(bufferSize, 0.0);
  rAudio.assign(bufferSize, 0.0);

  gui.add(grab_radius_1.setup("grab 1", 500, 0, 700));
  gui.add(grab_radius_2.setup("grab 2", 500, 0, 700));

  cur_angle = 0;

  sample_lenght = 512;
  step_by_tour  = 360.0/sample_lenght;

  center.set(ofGetWidth()/2.0f, ofGetHeight()/2.0f);
}

//--------------------------------------------------------------
void ofApp::drawGrab(){
  if (draw_grab){
    ofPushStyle();
    ofNoFill();
    ofSetLineWidth(1);
    ofSetColor(255);
    ofCircle(center, grab_radius_1);
    ofCircle(center, grab_radius_2);
    ofFill();
    ofPopStyle();
  }
}

//--------------------------------------------------------------
void ofApp::drawAudio(){
  if (draw_audio){
    int posx = ofGetWidth() * 0.1;
    int posy = ofGetHeight() * 0.7;
    int sizex = ofGetWidth() * 0.8;
    int sizey = ofGetHeight() * 0.2;

    ofPushStyle();
    ofPushMatrix();

    ofTranslate(posx, posy, 0);
  
    ofSetColor(225);
    ofDrawBitmapString("Left Channel", 4, 18);
    ofFill();
    ofSetLineWidth(1);	
    ofSetColor(255, 255, 255, 150);
    ofRect(0, 0, sizex, sizey);

    ofNoFill();  
    ofSetColor(245, 58, 135);
    ofSetLineWidth(2);
					
    ofBeginShape();
    for (unsigned int i = 0; i < lAudio.size(); i++){
      float x =  ofMap(i, 0, lAudio.size(), 0, sizex, true);
      ofVertex(x,  sizey * lAudio[i] + (sizey/2));
    }
    ofEndShape(false);
    ofSetColor(58, 135, 245);
    ofBeginShape();
    for (unsigned int i = 0; i < rAudio.size(); i++){
      float x =  ofMap(i, 0, rAudio.size(), 0, sizex, true);
      ofVertex(x, rAudio[i] * sizey + (sizey/2));
    }
    ofEndShape(false);

    ofPopMatrix();
    ofPopStyle();
  }
}

//--------------------------------------------------------------
void ofApp::audioOut(float * output, int bufferSize, int nChannels){
  pan = 0.5f;
  float leftScale = 1 - pan;
  float rightScale = pan;
  
  if (mode_audio){
    
    volume = 0.5;
    float a;
    
    for (int i = 0; i < bufferSize; i++){
      
	ofPoint grab_pos_1;
	ofPoint grab_pos_2;
	a = cur_angle * step_by_tour;
	
	cur_angle = (cur_angle+1) % sample_lenght;
	
	grab_pos_1.set(center.x + cos(a*PI/180)*grab_radius_1, 
		       center.y + sin(a*PI/180)*grab_radius_1);
	grab_pos_2.set(center.x + cos(a*PI/180)*grab_radius_2, 
		       center.y + sin(a*PI/180)*grab_radius_2);   
	
	ofColor grab_color_1 = render.getColor(grab_pos_1.x, grab_pos_1.y);
	ofColor grab_color_2 = render.getColor(grab_pos_2.x, grab_pos_2.y);

	lAudio[i] = output[i*nChannels    ] = ofMap(grab_color_1.r, 0, 255, -1, 1) * volume * leftScale;
	rAudio[i] = output[i*nChannels +1 ] = ofMap(grab_color_2.b, 0, 255, -1, 1) * volume * leftScale;      
    }
  }
}
