#pragma once

#include "ofMain.h"
#include "ofxGui.h"

#include "ofxOscParameterSync.h"



class ofApp : public ofBaseApp{

 public:
  void setup();
  void update();
  void draw();
  
  void keyPressed(int key);
  void keyReleased(int key);
  void mouseMoved(int x, int y );
  void mouseDragged(int x, int y, int button);
  void mousePressed(int x, int y, int button);
  void mouseReleased(int x, int y, int button);
  void mouseEntered(int x, int y);
  void mouseExited(int x, int y);
  void windowResized(int w, int h);
  void dragEvent(ofDragInfo dragInfo);
  void gotMessage(ofMessage msg);

  ofVec2f DeJong(float x, float y);
  
  ofImage render;


  int loc_x;
  int loc_y;
  int ind;

  ofVec2f out;
  
  int width;
  int height;
 
  ofxOscParameterSync sync;

  ofxPanel gui;

  ofParameterGroup parameters;
  
  ofxFloatSlider a;
  ofxFloatSlider b;
  ofxFloatSlider c;
  ofxFloatSlider d;
  ofxFloatSlider e;
  ofxFloatSlider f;
  ofxFloatSlider g;
  ofxFloatSlider h;

  float at, bt, ct, dt, et, ft, gt, ht;

  ofParameter<bool> mode_col;
  ofxFloatSlider hue_min;
  ofxFloatSlider hue_max;

  ofxIntSlider n_points;
  ofxFloatSlider decrease;
  ofxIntSlider fps;

  float x;
  float y;


  /*********/
  /* AUDIO */
  /*********/

  void setupAudio();
  void audioOut(float * input, int bufferSize, int nChannels);
  void drawAudio();
  void drawGrab();

  ofParameterGroup audio_params;

  ofParameter<bool> mode_audio;
  ofParameter<bool> draw_grab;
  ofParameter<bool> draw_audio;

  int bufferSize;
  int sampleRate;
  float volume;

  ofSoundStream soundStream;

  vector <float> lAudio;
  vector <float> rAudio;

  ofxIntSlider grab_radius_1;
  ofxIntSlider grab_radius_2;

  int grab_angle;
  int cur_angle;

  int sample_lenght;
  float step_by_tour;

  float volume_angle;
  ofPoint volume_pos;  
  float 	pan;

  ofPoint center;

  /*************/
  /* FIN AUDIO */
  /*************/
  
};
